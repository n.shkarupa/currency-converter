import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { CurrencyRates } from '../models/currency-rates';

import { environment } from '../../../environments/environment';

@Injectable()
export class CurrencyService {
	
	private url = `${environment.api.URL}?access_key=${environment.api.ACCESS_KEY}&symbols=${environment.api.CURRENCY}`;

  constructor(private http: Http) { }

	getRates(): Observable<CurrencyRates> {
		return this.http
			.get(this.url)
			.map((response: Response) => response.json().rates)
			.catch((error: any) => Observable.throw(error.json()));
	}
}
