export interface Record {
	amount: number;
	currency: string;
	inUSD: number;
}
