export interface CurrencyRates {
	USD: number,
	EUR: number,
	CZK: number
}
