import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConvertorTableComponent } from './convertor-table.component';

describe('ConvertorTableComponent', () => {
  let component: ConvertorTableComponent;
  let fixture: ComponentFixture<ConvertorTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConvertorTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConvertorTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
