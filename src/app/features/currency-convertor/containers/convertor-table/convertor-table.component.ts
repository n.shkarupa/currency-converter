import { Component, Input } from '@angular/core';

import { Record } from '../../../../core/models/record';

@Component({
  selector: 'app-convertor-table',
  template: `
	<table id="table" class="table">
		<tr class="table__row">
			<th>Amount</th>
			<th>Currency</th> 
			<th>in USD</th>
		</tr>
		<tr *ngFor="let record of records" class="table__row">
			<td>{{ record.amount }}</td>
			<td>{{ record.currency }}</td>
			<td>{{ record.inUSD }}</td>
		</tr>
	</table>
	<p class="total">Total: <b>{{total}}$</b></p>
  `,
  styleUrls: ['./convertor-table.component.scss']
})
export class ConvertorTableComponent {

	@Input('records') records: Record[];
	@Input('total') total: number;

}
