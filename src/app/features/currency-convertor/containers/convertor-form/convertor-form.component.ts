import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

import { Record } from '../../../../core/models/record';
import { CurrencyRates } from '../../../../core/models/currency-rates';

import { ToUsdPipe } from '../../../../shared/pipes/to-usd.pipe';


@Component({
  selector: 'app-convertor-form',
  template: `
	<form #form="ngForm" class="form" novalidate>
		<fieldset>
			<legend class="form__legend">
				Multi-Currency Converter
			</legend>

			<div class="form__row">
				<input
					name="amount"
					class="input"
					[ngClass]="{'input_invalid': input_error}"
					type="text"
					placeholder="Enter Amount"
					(input)="validate($event); onChange($event)"
					required
					[(ngModel)]="record.amount">
			</div>

			<div class="form__row">
				<select
					name="currency"
					class="select"
					required
					[(ngModel)]="record.currency"
					(change)="onChange($event)">
					<option *ngFor="let currency of currencies"
						[ngValue]="currency"
						[selected]="currency == selected_currency">
						{{ currency }}
					</option>
				</select>
			</div>
			
			<div class="form__row">
				<span>In USD: <b>{{ hint }}$</b></span>
				<button class="submit-button"
					type="submit"
					[disabled]="form.invalid || input_error"
					(click)="addRecord($event)">
					submit
				</button>
			</div>
		</fieldset>
	</form>
  `,
	styleUrls: ['./convertor-form.component.scss'],
	providers: [
		ToUsdPipe
	]
})
export class ConvertorFormComponent implements OnInit {

	@Input('rates') rates: CurrencyRates;
	@Input('currencies') currencies: string[];
	@Input('selected_currency') selected_currency: string;

	@Output('add_record') create_record: EventEmitter<Record> = new EventEmitter();

	record = {
		amount: '',
		currency: ''
	};
	input_error: boolean;
	hint: string;

	constructor(private toUsdPipe: ToUsdPipe){
		this.input_error = false;
		this.hint = '0';
	}

	ngOnInit(): void {
		this.record.currency = this.selected_currency;
	}

	validate(event){
		// Allows negative and float values
		if(/^-?\d*((\.|\,)(?=\d))?\d*$/.test(this.record.amount) && this.record.amount != '0'){
			this.input_error = false;
			return;
		}
		this.input_error = true;
	}

	onChange(event) {
		let res = this.toUsdPipe.transform(this.record.amount, this.rates, this.record.currency);
		if (res){
			this.hint = '' + res;
			return;
		}
		this.hint = '0';
	}

	addRecord(event){
		var new_record: Record = {
			amount: +this.record.amount,
			currency: this.record.currency,
			inUSD: +this.hint
		}
		this.record.amount = '';
		this.hint = '0';
		this.create_record.emit(new_record);
	}

}
