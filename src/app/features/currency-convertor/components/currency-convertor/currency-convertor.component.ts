import { Component, OnInit } from '@angular/core';
import { CurrencyService } from '../../../../core/services/currency.service';
import { CurrencyRates } from '../../../../core/models/currency-rates';

import { Record } from '../../../../core/models/record';

@Component({
  selector: 'app-currency-convertor',
	template: `
	<div class="container">
		<div class="container__row">
			<app-convertor-form
				[rates]="rates"
				[currencies]="currencies"
				[selected_currency]="'EUR'"
				(add_record)="addRecord($event)">
			</app-convertor-form>
		</div>
		<div class="container__row">
			<app-convertor-table
				[records]="records"
				[total]="total">
			</app-convertor-table>
		</div>
	</div>
  `,
  styleUrls: ['./currency-convertor.component.scss']
})
export class CurrencyConvertorComponent implements OnInit {

	rates: CurrencyRates;
	currencies = ['USD', 'EUR', 'CZK'];
	records: Record[] = [];
	total: number = 0;

  constructor(private currencyService: CurrencyService) { }

  ngOnInit() {
		this.currencyService.getRates()
			.subscribe((data: any) => this.rates = data);
  }
	
	addRecord(new_record: Record){
		this.total += new_record.inUSD;
		this.total = +this.total.toFixed(2);
		this.records = [...this.records, new_record];
	}
}
