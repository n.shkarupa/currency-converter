import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// Modules
import { SharedModule } from '../../shared/shared.module';
// Components
import { CurrencyConvertorComponent } from './components/currency-convertor/currency-convertor.component';
// Containers
import { ConvertorFormComponent } from './containers/convertor-form/convertor-form.component';
import { ConvertorTableComponent } from './containers/convertor-table/convertor-table.component';

@NgModule({
  imports: [
		CommonModule,
		FormsModule,
		SharedModule
  ],
	declarations: [
		CurrencyConvertorComponent,
		ConvertorFormComponent,
		ConvertorTableComponent
	],
	exports: [
		CurrencyConvertorComponent
	]
})
export class CurrencyConvertorModule { }
