import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';

import { CurrencyService } from './core/services/currency.service';

import { CurrencyConvertorModule } from './features/currency-convertor/currency-convertor.module';

import { AppComponent } from './app.component';

const routes: Routes = [
	{ path: 'home', component: AppComponent },
	{ path: '**',  redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  declarations: [AppComponent],
  imports: [
		BrowserModule,
		CurrencyConvertorModule,
		RouterModule.forRoot(routes),
		HttpModule
  ],
  providers: [CurrencyService],
	bootstrap: [AppComponent]
})
export class AppModule { }
