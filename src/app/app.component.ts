import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
	template: `
		<app-currency-convertor></app-currency-convertor>
	`
})
export class AppComponent {

}
