import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyRates } from '../../core/models/currency-rates';

@Pipe({
  name: 'toUsd'
})
export class ToUsdPipe implements PipeTransform {

	transform(amount: string, rates: CurrencyRates, currency: string): number {
		var res = parseFloat(amount);
		if(rates){
			switch(currency) {
				case 'CZK':
					res = res / rates.CZK * rates.USD;
				case 'EUR':
					res = res * rates.USD;
			}
		}
		res = +res.toFixed(2);
		return res;
  }

}
