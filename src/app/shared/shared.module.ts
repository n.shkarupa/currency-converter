import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToUsdPipe } from './pipes/to-usd.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
	declarations: [ToUsdPipe],
	exports: [ToUsdPipe]
})
export class SharedModule { }
